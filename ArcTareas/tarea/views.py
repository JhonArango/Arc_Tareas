from django.shortcuts import render
from django.views.generic import TemplateView,DetailView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from tarea.models import Tarea
from django.http import JsonResponse
from tarea.forms import TareaForm
# Create your views here.

@method_decorator(login_required, name='dispatch')
class HomeView(TemplateView):
    template_name = "tarea/home.html"

    def get_context_data(self,*args,**kwargs):
        tareas = Tarea.objects.filter(user__username=self.request.user)
        return {'tareas':tareas}


@method_decorator(login_required, name='dispatch')
class TareaDetailView(DetailView):
    model=Tarea
    template_name = "tarea/tarea_detail.html"

class TareaEliminar(TemplateView):
    
    def post(self,request,*args,**kwargs):
        pk = request.POST['identificador']
        print(pk)
        tarea = Tarea.objects.get(pk=pk)
        tarea.delete()
        response = {}
        return JsonResponse(response)

class TareaAgregar(TemplateView):
    def post(self,request,*args,**kwargs):
        name = request.POST['name']
        content = request.POST['content']
        tarea = Tarea.objects.create(user=self.request.user,
                                     name=name,content=content,time=0)
        response = {}
        return JsonResponse(response)

class GuardarTiempo(TemplateView):
    def post(self,request,*args,**kwargs):
        tiempo = request.POST['tiempo']
        id_tarea = request.POST['id_tarea']
        tarea = Tarea.objects.get(pk=id_tarea)
        tarea.time = tarea.time + int(tiempo)
        tarea.save()
        response = {}
        return JsonResponse(response)