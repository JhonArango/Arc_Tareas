from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Tarea(models.Model):
    """Model definition for Tarea."""
    # TODO: Define fields here
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(verbose_name="Nombre", max_length=50)
    content = models.TextField(verbose_name="Contenido")
    time = models.IntegerField(verbose_name="Tiempo de Tarea")
    created = models.DateTimeField(verbose_name="Fecha de creacion",auto_now_add=True)
    updated = models.DateTimeField(verbose_name="Fecha de actualizacion",auto_now=True)

    class Meta:
        """Meta definition for Tarea."""

        verbose_name = 'Tarea'
        verbose_name_plural = 'Tareas'
        ordering = ['created']

    def __str__(self):
        """Unicode representation of Tarea."""
        return self.name
