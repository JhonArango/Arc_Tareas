from django.contrib import admin
from django.urls import path,include
from tarea.views import HomeView,TareaDetailView,TareaEliminar,TareaAgregar,GuardarTiempo

app_name = "tarea"

urlpatterns = [
    path('tarea_list/',HomeView.as_view(),name='tarea_list'),
    path('tarea_detail/<int:pk>',TareaDetailView.as_view(),name="detail"),
    path('eliminar_tarea/',TareaEliminar.as_view(),name="eliminar_tarea"),
    path('agregar_tarea/',TareaAgregar.as_view(),name="agregar_tarea"),
    path('guardar_tiempo/',GuardarTiempo.as_view(),name="guardar_tiempo"),
]
