from django.contrib import admin
from tarea.models import Tarea

# Register your models here.
class TareaAdmin(admin.ModelAdmin):
    '''Admin View for '''
    readonly_fields = ('created','updated')

admin.site.register(Tarea,TareaAdmin)
