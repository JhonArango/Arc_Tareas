from django import forms

class TareaForm(forms.Form):
    name = forms.CharField(label="name",max_length=250)
    content = forms.CharField(max_length=250)

