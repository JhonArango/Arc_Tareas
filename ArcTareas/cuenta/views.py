from django.shortcuts import render,redirect
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.models import User
from cuenta.forms import LoginForm,RegisterForm,RecuperarForm,CambioForm
from django.urls import reverse_lazy
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import datetime
# Create your views here.
class LoginView(FormView):
    template_name = "cuenta/login.html"
    form_class = LoginForm
    success_url  = '/tarea/tarea_list/'

    def form_valid(self,form):
        user = User.objects.filter(username=self.request.POST['username']).first()
        if user is None:
            return render(self.request, self.template_name, {'form': form,'vacio':True})
        confirm_user = authenticate(username=user.username, password=self.request.POST['password'])
        if confirm_user:
            login(self.request, user)
            return super().form_valid(form)
        else:
            return render(self.request, self.template_name, {'form': form,'error':True})

    def form_invalid(self,form):
        return render(self.request, self.template_name, {'form': form,'invalido':True})
         
class RegisterView(TemplateView):
    template_name = "cuenta/singup.html"
    form = RegisterForm

    def get_context_data(self,*args,**kwargs):
        return {'form':self.form}

    def post(self,request,*args,**kwargs):
        form = self.form(request.POST)
        if form.is_valid():
            if len(User.objects.filter(username=request.POST['username'])) == 0:
                user = User.objects.create(username=request.POST['username'],
                                            email=request.POST['email'],
                                            password=request.POST['password'])
                return redirect("loginI")
        return render(request, self.template_name, {'form': form,'error':True})   
        
def logout_view(request):
    logout(request)
    return redirect("loginI")
