from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label="Username",max_length=250)
    password = forms.CharField(max_length=250)

class RegisterForm(forms.Form):
    username = forms.CharField(label="Username",max_length=250,required=True)
    email = forms.EmailField(label="email", required=True)
    password = forms.CharField(max_length=250, required=True)

class RecuperarForm(forms.Form):
      email = forms.EmailField(label="email", required=True)

class CambioForm(forms.Form):
      email_old = forms.EmailField(label="email_old", required=True)
      email_new1 = forms.EmailField(label="email_new1", required=True)
      email_new2 = forms.EmailField(label="email_new2", required=True)


