from django.contrib import admin
from django.urls import path,include,re_path
from cuenta.views import LoginView,RegisterView,logout_view
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('',LoginView.as_view(),name='loginI'),
    path('singup/',RegisterView.as_view(),name='singup'),
    path('logout/',logout_view,name='logout'),
    path('password_reset/', auth_views.password_reset, 
        {'email_template_name':'cuenta/password_reset_email.html',
        'template_name':'cuenta/password_reset_form.html',
        'subject_template_name':'cuenta/password_reset_subject.txt'},
        name='password_reset'),
    path('password_reset_done/', auth_views.password_reset_done, 
        {'template_name':'cuenta/password_reset_done.html'},
        name='password_reset_done'),

    re_path('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
        auth_views.password_reset_confirm,
        {'template_name':'cuenta/password_reset_confirm.html'},
        name='password_reset_confirm'),

    path('password_reset_complete/',auth_views.password_reset_complete,
        {'template_name':'cuenta/password_reset_complete.html'},
        name='password_reset_complete'),
]
