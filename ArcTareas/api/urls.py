from django.urls import path
from api.views import listTarea,crearTarea

urlpatterns = [
    path('list/',listTarea,name='list'),
    path('create/',crearTarea,name='create'),   
]