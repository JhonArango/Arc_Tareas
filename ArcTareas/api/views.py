from rest_framework import status 
from rest_framework.decorators import api_view
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt 

from django.contrib.auth.models import User
from tarea.models import Tarea
from api.serializers import TareaSerializer
from django.conf import settings

notAuthorizedMessage = 'You are not authorized to do this process'
dataResponse = {}


@api_view(['GET']) 
def listTarea(request):
    statusResponse = status.HTTP_200_OK

    if request.method == 'GET':
        tareas = Tarea.objects.all()

        if len(tareas) == 0:
            statusResponse = status.HTTP_404_NOT_FOUND,
            dataResponse = {
                            'detail': 'Not found',
                            'errCode': 'RNE001'
            }
        else: 
            tareaList = []
            for index in range(0, len(tareas)):
                tarea = tareas[index]

                tareaList.append({
                    'user':{
                        'username':tarea.user.username,
                        'email':tarea.user.email,
                    },
                    'name': tarea.name,
                    'content': tarea.content,
                    'time': tarea.time,
                })

            dataResponse = {
                'listData': tareaList,
            }
    else:
            statusResponse = status.HTTP_400_BAD_REQUEST
            dataResponse = {
                'detail': 'Request data is not valid',
                'errCode': 'NVD0001'
            }

    return JsonResponse(
        dataResponse, 
        safe=False, 
        status=statusResponse
    )

        
@csrf_exempt
@api_view(['POST']) 
def crearTarea(request):

    AUTH_APP_T = request.META.get('HTTP_DB_TOKEN_APP')

    if AUTH_APP_T is None or AUTH_APP_T != settings.AUTH_APP_TOKEN:
        dataResponse = {
            'detail': notAuthorizedMessage,
            'errCode': 'NVTA001'
        }
        statusResponse = status.HTTP_403_FORBIDDEN

        return JsonResponse(
            dataResponse,
            safe=False,
            status=statusResponse
        )

    if request.method == 'POST':
        data=request.POST
        serializer = TareaSerializer(data=data)

        if serializer.is_valid():
            user = User.objects.get(id=request.data.get('user'))
            name = request.data.get('name')
            content = request.data.get('content')
            time = request.data.get('time')

            tarea = Tarea(
                user=user,
                name=name,
                content=content,
                time=time
                )

            tarea.save()

            statusResponse = status.HTTP_201_CREATED
            dataResponse = {
                'detail': 'Tarea creada',
                'Tarea': tarea.name,
            }
        else:
            statusResponse = status.HTTP_400_BAD_REQUEST
            dataResponse = {
                'detail': 'Request data is not valid',
                'errCode': 'NVD0001'
            }
    else:
        statusResponse = status.HTTP_405_METHOD_NOT_ALLOWED
        dataResponse = {
                'detail': 'Method Not Allowed',
                'errCode': 'MNA0001'
            }

    return JsonResponse(
        dataResponse, 
        safe=False, 
        status=statusResponse
    )